/*
* IceBreaker
* Copyright (c) 2000 Matthew Miller <mattdm@mattdm.org> http://www.mattdm.org/
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the Free
* Software Foundation; either version 2 of the License, or (at your option)
* any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
* for more details.
*
* You should have received a copy of the GNU General Public License along
* with this program; if not, write to the Free Software Foundation, Inc., 59
* Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
*/


#include <SDL.h>
#include <stdlib.h>
#include "icebreaker.h"
#include "globals.h"
#include "laundry.h"

static SDL_Rect laundrylist[MAXDIRTY];
static int laundrycount;

void initlaundry()
{
	laundrycount=0;
}


void soil(SDL_Rect r)
{ // makes stuff dirty, of course.
	if (laundrycount<MAXDIRTY) // it's good to have this check here, but
	{                          // since this is the most-used function in
	                           // the whole program, it might be worth removing
	                           // for production builds...
		laundrylist[laundrycount] = r;
		laundrycount++;
	}
	else
	{
		fprintf(stderr, "Too much dirty laundry!\n");
		exit(1);
	}
}

void clean()
{
	SDL_UpdateRects(screen, laundrycount, laundrylist);
	//SDL_UpdateRect(screen,0,0,0,0);
	laundrycount=0;
	
}
